package ru.gazpromneft.processors;

import lombok.extern.log4j.Log4j2;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

@Log4j2
public class DelayCountProcessor implements Processor {
    Long maxDelay = 0L;
    @Override
    public void process(Exchange exchange) throws Exception {

        Long counter = (Long) exchange.getIn().getHeader("XXX-MES-COUNT");
        String startTime = (String) exchange.getIn().getHeader("XXX-MES-START");
        LocalDateTime parsed = LocalDateTime.parse(startTime);
//        log.info("XXX-MES-COUNT: " + counter + " XXX-MES-START: " + parsed);
        LocalDateTime endTime = LocalDateTime.now();
//        log.info("End time: " + endTime);
        Long delay = ChronoUnit.MILLIS.between(parsed, endTime);
        compareDelay(delay, counter);
        log.info("DELAY: "+ delay +  " Message : " + counter + " MaxDelay: " + maxDelay);
    }

    private void compareDelay(Long delay, Long counter){
        if (delay > maxDelay){
            log.info("MAX DELAY: " +delay + " Message: " + counter );
            maxDelay = delay;
        }
    }
}
