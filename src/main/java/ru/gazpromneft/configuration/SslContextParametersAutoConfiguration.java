package ru.gazpromneft.configuration;

import org.apache.camel.util.jsse.KeyManagersParameters;
import org.apache.camel.util.jsse.KeyStoreParameters;
import org.apache.camel.util.jsse.SSLContextParameters;
import org.apache.camel.util.jsse.TrustManagersParameters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * Настройка SSL HTTP-эндпоинта
 */
@Configuration
public class SslContextParametersAutoConfiguration {

    @Bean(name = "sslContextParameters")
    public SSLContextParameters sslContextParameters(Environment environment) {
        KeyStoreParameters trustParams = new KeyStoreParameters();
        trustParams.setResource(environment.getProperty("server.ssl.trust-store"));
        trustParams.setPassword(environment.getProperty("server.ssl.trust-store-password"));

        TrustManagersParameters trustManParams = new TrustManagersParameters();
        trustManParams.setKeyStore(trustParams);

        KeyStoreParameters keyParams = new KeyStoreParameters();
        keyParams.setResource(environment.getProperty("server.ssl.key-store"));
        keyParams.setPassword(environment.getProperty("server.ssl.key-store-password"));

        KeyManagersParameters keyManParams = new KeyManagersParameters();
        keyManParams.setKeyStore(keyParams);
        keyManParams.setKeyPassword(keyParams.getPassword());

        SSLContextParameters sslContextParameters = new SSLContextParameters();
        sslContextParameters.setTrustManagers(trustManParams);
        sslContextParameters.setKeyManagers(keyManParams);

        return sslContextParameters;
    }
}
