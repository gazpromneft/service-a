package ru.gazpromneft.configuration;

import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.apache.activemq.jms.pool.PooledConnectionFactory;
import org.apache.camel.component.amqp.AMQPComponent;
import org.apache.qpid.jms.JmsConnectionFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration parameters filled in from applicationlocal.properties and overridden using env variables on Openshift.
 */

@Data
@Configuration
@ConfigurationProperties(prefix = "amqp")
@Log4j2
public class AMQPConfiguration {

    /**
     * AMQ remoteURI
     */
    private String remoteURI;

    /**
     * AMQ service host
     */
    private String host;

    /**
     * AMQ service port
     */
    private Integer port;

    /**
     * AMQ username
     */
    private String username;

    /**
     * Макс кол-во коннектов
     */
    private Integer maxConnections;

    /**
     * AMQ password
     */
    private String password;

    @Bean(name = "amqp-component")
    AMQPComponent amqpComponent(AMQPConfiguration config) {
        JmsConnectionFactory qpid = jmsConnectionFactory(config);
        PooledConnectionFactory factory = new PooledConnectionFactory();
        factory.setConnectionFactory(qpid);
        factory.setMaxConnections(maxConnections);
        AMQPComponent component = new AMQPComponent(factory);
        log.info("COMPONENT:" + component.getConfiguration().getClientId());
        return component;
    }
    @Bean
    JmsConnectionFactory jmsConnectionFactory(AMQPConfiguration config){
        JmsConnectionFactory qpid = new JmsConnectionFactory(config.getUsername(), config.getPassword(), config.getRemoteURI());
        qpid.setTopicPrefix("topic://");
        log.info("QPID user: " + qpid.getUsername() + "  "+ qpid.getPassword() + "  " +  qpid.getClientID()) ;
        return qpid;
    }

}
